<?php
/**
* Template Name: Home
 */

get_header(); ?>

</div>
</div>
</div>

<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active ">
    <div class="contenedor">
      <img class="d-block w-100" src="/tienda/wp-content/themes/tienda-1/img/imagen1.jpg"
        alt="First slide">
       <div class="carousel-caption"> 
            <h3 style="color:white" class="t-sli">NEW ARRIVAL</h3> 
            <div><button type="button" class="btn3 btn-primary b" data-toggle="modal" data-target="#exampleModal">
SHOP NOW
</button></div>

    </div> 
    </div>
    </div>
    <div class="carousel-item">
      <div class="contenedor">
      <img class="d-block w-100" src="/tienda/wp-content/themes/tienda-1/img/xx.jpg"
        alt="Second slide">
        <div class="carousel-caption"> 
            <h3 style="color:white" class="t-sli">EXPERIENCE</h3>
            <br>
            <div style="position:relative;display:inline-block;" class="tp-splitted tp-charsplit">ENJOY THE NEW FASHION</div> 
            <br>
            <br>
            <div><button type="button" class="btn3 btn-primary b" data-toggle="modal" data-target="#exampleModal">
READ HISTORY
</button></div>
          </div>
    </div>
    </div>
    <div class="carousel-item">
      <div class="contenedor">
      <img class="d-block w-100" src="/tienda/wp-content/themes/tienda-1/img/x1.jpg"
        alt="Second slide">
        <div class="carousel-caption"> 
            <h3 style="color:white"  class="t-sli">UPTO 70%</h3>
            <br>

            <div><button type="button" class="btn4 btn-primary b" data-toggle="modal" data-target="#exampleModal">
MORE DETAILS
</button></div> 
          </div>
    </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>



 <br>
 <br>

<div class="container">
<div class="row">
<div class="col-md-6">
<div class="contenedor seccion4">
<center class="containerback"><h1 class="hh1">DRESSES</h1></center>
<div class="row">
</div>
</div>
</div>

<div class="col-md-6">
<div class="contenedor seccion5">
<center class="containerback"><h1 class="hh1">BAGS</h1></center>
<div class="row">
</div>
</div>
</div>
</div>
</div>








<br>
<br>
<br>

<div class="container">
<div class="row">

<div class="col-sm-3 col-xs-6">
<div class="contenedor seccion1-1">
<center class="containerback2"><h4 class="hh2">LINGERIE</h4></center>

</div>
</div>

<div class="col-sm-3 col-xs-6 ">
<div class="contenedor seccion2-1">
<center class="containerback2"><h4 class="hh2">SHIRTS</h4></center>
</div>
</div>
<div class="col-sm-3 col-xs-6 ">
<div class="contenedor seccion3-1">
<center class="containerback2"><h4 class="hh2">SHOES</h4></center>
</div>
</div>
<div class="col-sm-3 col-xs-6 ">
<div class="contenedor seccion4-1">
  <center class="containerback2"><h4 class="hh2">DENIMS</h4></center>
</div>
</div>

</div>
</div>

<br>
<br>
<br>

<div class="container">
<div class="row">

<div class="col-sm-4">
</div>

<div class="col-sm-4">
</div>
<div class="col-sm-4">
</div>
<div class="col-sm-4">
</div>
<div class="col-sm-4">
	<center><h1>NEW ARRIVALS</h1></center>
	<center><h5>Lorem ipsum dolor sit amet, consectetur adipisicing.</h5></center>
</div>
</div>
</div>

<br>
<br>
<br>




<div class="container">
<div class="row">
<?php
        $args = array(
            'post_type' => 'product',
            'posts_per_page' => 8,
            );
        $loop = new WP_Query( $args );
        if ( $loop->have_posts() ) {
            while ( $loop->have_posts() ) : $loop->the_post();

            $featured_img_url = get_the_post_thumbnail_url($the_post->ID, 'full');


            $price = get_post_meta(get_the_ID(), '_regular_price', true);

            

            
?>
<div class="col-sm-3 col-xs-6">
<img class="tamano-img" class="/tienda/wp-content/themes/tienda-1/img/5-1.jpg" src="<?php echo $featured_img_url; ?>">
<div class="product-info clearfix">
<div class="product-rating product-rating-empty">
<div class="empty-rating">
</div>
</div>
<div class="product-title title-h6">
<center><h1 class="card-title titulo_productos"><?php echo get_the_title(); ?></h1></center>
</div>
<div class="product-price">
<center><span class="price">
<span class="woocommerce-Price-currencySymbol">
$<?php echo $price ?></span>
</span></center>
</div>
</div>
<br>
<center><a class="btn btn-primary btn-md boton_verMas" href="<?php the_permalink(); ?>">Ver Más</a></center>
</div>



<?php

            endwhile;
        } else {
            echo __( 'No se encontrarón productos' );
        }
        wp_reset_postdata();
    ?>

</div>
</div>

<br>
<br>
<br>
<div class="container">
<br>
<br>
<br>

<div class="container">
<div class="row">
<div class="col-md-6 seccion1-1 tel">

</div>
<div class="col-md-6 seccion2-2">
<div class="row">

<div class="col-sm-4">
</div>

<div class="col-sm-4">
</div>
<div class="col-sm-4">
</div>
<div class="col-sm-4">
</div>
<div class="col-sm-4">
  <br>
  <br>
  <center><h1 style="color: #000000;">BAGS</h1></center>
  <br>
  <br>
  <center><span style="color: #000000;">Lorem ipsum dolor sit amet, consectetur adipisicing.</span></center>
  <br>
  <br>
<center><div><button type="button" class="btn3 btn-primary b" data-toggle="modal" data-target="#exampleModal">
SHOP NOW
</button></div></center>
</div>
</div>
</div>
</div>
</div>

<br>
<br>
<br>

</div>

<br>
<br>
<br>

</div>




<br>
<br>
<br>


<div class="row">
<div class="contenedor">
</div>

<div class="col-sm-3  ta-img img-1">
<div class="contenedor">
<center><h1>FREE SHIPPING</h1></center>
<br>

<center><p>Duis sed odio sit amet</p></center>
<br>
</div>
</div>
<div class="col-sm-3   ta-img img-2">
<div class="contenedor">
<center><h1>GUARANTEE SERVICE</h1></center>
<br>
<center><p>Duis sed odio sit amet </p></center>
</div>
</div>

<div class="col-sm-3 fta-img img-3">
<div class="contenedor">
<center><h1>100% MONEY BACK</h1></center>
<br>
<center><p>Duis sed odio sit amet</p></center>
</div>
</div>

<div class="col-sm-3   ta-img img-4">
<div class="contenedor">
<center><h1>PRODUCTS OFFERS</h1></center>
<br>
<center><p>Lorem ipsum dolor sit amet</p></center>
</div>
</div>

</div>






<br>
<br>
<br>

<div class="row">
<div class="col-md-6 azul2">

</div>
<div class="col-md-6 azul">
<div class="row">

<div class="col-sm-4">
</div>

<div class="col-sm-4">
</div>
<div class="col-sm-4">
</div>
<div class="col-sm-4">
</div>
<div class="col-sm-4">
  <br>
  <br>
  <center><h1 style="color: #000000;">BAGS</h1></center>
  <br>
  <br>
  <center><span style="color: #000000;">Lorem ipsum dolor sit amet, consectetur adipisicing.</span></center>
  <br>
  <br>
<center><div><button type="button" class="btn3 btn-primary b" data-toggle="modal" data-target="#exampleModal">
SHOP NOW
</button></div></center>
</div>
</div>
</div>
</div>




</div>
</div>


<div class="slider_video">
  <div class="overlay"></div>
  <video playsinline="playsinline" autoplay="autoplay" muted="muted" loop="loop">
    <source src="https://storage.googleapis.com/coverr-main/mp4/Mt_Baker.mp4" type="video/mp4">
  </video>
   <div class="row">
<div class="col-sm-4">
</div>
<div class="col-sm-4 pa white-slider">
<center><h1 style="color:#fff">SHIPPING METHODS.</h1></center>
</div>
<div class="col-sm-4">
</div>



</div>
</div>

<div class="slider">
 <div class="img_slider3">
 <div class="row">
<div class="col-sm-4">
</div>
<div class="col-sm-4 pa">
<h1>NEW ARRIVALS</h1>
<center><h5>Lorem ipsum dolor sit amet, consectetur adipisicing.</h5></center>
<center><div><button type="button" class="btn2 btn-primary b" data-toggle="modal" data-target="#exampleModal">
Add Cart
</button></div></center>
</div>
<div class="col-sm-4">
</div>



</div>
 </div>

</div>

<br>
<br>
<br>

<div class="container">
<div class="row">

<div class="col-sm-4">
</div>

<div class="col-sm-4">
</div>
<div class="col-sm-4">
</div>
<div class="col-sm-4">
</div>
<div class="col-sm-4">
	<center><h1>LATEST NEWS</h1></center>
	<center><h5>Lorem ipsum dolor sit amet, consectetur adipisicing.</h5></center>
</div>
</div>
</div>


<br>
<br>
<br>


<script>

jQuery(document).ready(function(){

 jQuery('.multiple-items').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 3
});
  
});


</script>






<br>
<br>
<br>


<div class="row">
<div class="col-md-6 seccion1">
<div class="contenedor">
<center><h1>WE ACCEPT</h1></center>
<center><p>Lorem ipsum dolor sit amet,elit, sed do eiusmod t ut labore et</p></center>
<div class="row">
<div class="col-sm-4 tarjetas">
<img src="/tienda/wp-content/themes/tienda-1/img/trajeta1.jpg">
</div>
<div class="col-sm-4 tarjetas">
<img src="/tienda/wp-content/themes/tienda-1/img/tarjeta2.jpg">
</div>
<div class="col-sm-4 tarjetas">
<img src="/tienda/wp-content/themes/tienda-1/img/tarjeta3.jpg">
</div>
</div>
<br>
<div class="row">
<div class="col-sm-4 tarjetas">
<img src="/tienda/wp-content/themes/tienda-1/img/tarjeta4.jpg">
</div>
<div class="col-sm-4 tarjetas">
<img src="/tienda/wp-content/themes/tienda-1/img/tarjeta5.jpg">
</div>
<div class="col-sm-4 tarjetas">
<img src="/tienda/wp-content/themes/tienda-1/img/tarjeta6.jpg">
</div>
</div>
<br>
<div class="row">
<div class="col-sm-4 tarjetas">
<img src="/tienda/wp-content/themes/tienda-1/img/trajeta7.jpg">
</div>
<div class="col-sm-4 tarjetas">
<img src="/tienda/wp-content/themes/tienda-1/img/tarjeta8.jpg">
</div>
<div class="col-sm-4 tarjetas">
<img src="/tienda/wp-content/themes/tienda-1/img/trajeta9.jpg">
</div>
</div>
</div>
</div>
<div class="col-md-6 seccion2">
<div class="contenedor">
<center><h1>SHIPPING METHODS</h1></center>
<br>

<center><p>Lorem ipsum dolor sit amet</p></center>
<br>
<center><p>Lorem ipsum dolor sit amet</p></center>
<br>
<center><p>Lorem ipsum dolor sit amet</p></center>
<br>
<center><p>Lorem ipsum dolor sit amet</p></center>

</div>
</div>
</div>






<br>
<br>
<br>


<div class="back-1">
<div class="row">
<div class="col-sm-3 ">
<h1 style="color:white;">CONTACT</h1>
<div style="color:white;" class="gem-contacts-item gem-contacts-address">
Address:<br> 908 New Hampshire Avenue #100, 
Washington, DC 20037, United States</div>
<div style="color:white;" class="gem-contacts-item gem-contacts-phone">
Phone: <a href="tel:+1 916-875-2235">+1 916-875-2235</a></div>
<div style="color:white;" class="gem-contacts-email">Email: <a href="mailto:info@domain.tld">info@domain.tld</a></div>
<div  style="color:white;"class="gem-contacts--website">Website: <a href="http://www.codex-themes.com">www.codex-themes.com</a></div>
</div>
<div class="col-sm-3">
<h1 style="color:white;">RECENT POSTS</h1>
<span class="glyphicon">&#x2601;</span>
<div class="gem-pp-posts-item"><a style="color:white;"href="https://codex-themes.com/thegem/proin-gravida-nibh-velit-auctor-aliquet-aenean-3/">Proin gravida nibh velit auctor aliquet aenean...</a></div>
<br>
<span class="glyphicon">&#x2601;</span>
<div class="gem-pp-posts-item"><a style="color:white;"href="https://codex-themes.com/thegem/proin-gravida-nibh-velit-auctor-aliquet-aenean-3/">Proin gravida nibh velit auctor aliquet aenean...</a></div>
<br>
<span class="glyphicon">&#x2601;</span>
<div class="gem-pp-posts-item"><a style="color:white;"href="https://codex-themes.com/thegem/proin-gravida-nibh-velit-auctor-aliquet-aenean-3/">Proin gravida nibh velit auctor aliquet aenean...</a></div>
</div>
<div class="col-sm-3">
<h1 style="color:white;">EMAIL US</h1>

<?php echo do_shortcode('[contact-form-7 id="166" title="New-form"]'); ?>




</div>
<div class="col-sm-3">
<h1 style="color:white;">LATEST PROJECTS</h1>
<div class="row">
<div class="col-sm-4 tarjetas2">
<img src="/tienda/wp-content/themes/tienda-1/img/1.jpg">
</div>
<div class="col-sm-4 tarjetas2">
<img src="/tienda/wp-content/themes/tienda-1/img/1.jpg">
</div>
<div class="col-sm-4 tarjetas2">
<img src="/tienda/wp-content/themes/tienda-1/img/1.jpg">
</div>
</div>
<br>
<div class="row">
<div class="col-sm-4 tarjetas2">
<img src="/tienda/wp-content/themes/tienda-1/img/1.jpg">
</div>
<div class="col-sm-4 tarjetas2">
<img src="/tienda/wp-content/themes/tienda-1/img/1.jpg">
</div>
<div class="col-sm-4 tarjetas2">
<img src="/tienda/wp-content/themes/tienda-1/img/1.jpg">
</div>
</div>
<br>
<div class="row">
<div class="col-sm-4 tarjetas2">
<img src="/tienda/wp-content/themes/tienda-1/img/1.jpg">
</div>
<div class="col-sm-4 tarjetas2">
<img src="/tienda/wp-content/themes/tienda-1/img/1.jpg">
</div>
<div class="col-sm-4 tarjetas2">
<img src="/tienda/wp-content/themes/tienda-1/img/1.jpg">
</div>
</div>


</div>
</div>
</div>





















<?php
get_footer();
