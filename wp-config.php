<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'tienda' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '=>prTxrZ,j!bRA]CNY.zo+]tJXG1+Zd/{3>19[/!zI6Ugs:eh9,mAAS_GS5c{s} ' );
define( 'SECURE_AUTH_KEY',  'Fy%CC^;za_T^?FjT{_1Gwy<Cp>ipMrp.]YLngr)tli+7NVwy<M#RNCcFQ~LA!M?w' );
define( 'LOGGED_IN_KEY',    '7fe$KzgIq9_v|wgnM=(^* zdUdpSmp mZ_H&3D:N*52RK`Ax*gMc;B(o9fmJn]Uc' );
define( 'NONCE_KEY',        'EN=zzVpEM>w-N`zZUN*:CK#D4HBaR/?Y)*Z.$6}t-V$7SlohHS@W@U$yE?gjy[r*' );
define( 'AUTH_SALT',        '79FCz8wUx+LGN+ ne^3+NJeRN5Nh.MG,E33{r=H[n8M*9A 6FIzDGH}-QpUsE{&G' );
define( 'SECURE_AUTH_SALT', '_$b]% 6+,7I]*!bkmRr1um$$m&MoaH_K|5mRmSI7nk^q_xiib]g)Bq$Q+%fP;n[t' );
define( 'LOGGED_IN_SALT',   '^gU8&]JXh-kV>:*r8R-q$Ef,%9:U]T<`R:xPE d.)]NFQH]_l#Ozc-(Rs95(@mk`' );
define( 'NONCE_SALT',       ';&W^P!.+Tg)r{[q&v j5u.Oa](69u/qJEqA(peAUE+u`)7TA/w2E*]Ux,<YDG#Ta' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
 // Activar modo WP_DEBUG
define('WP_DEBUG', true);

// Activar registro de depuración al fichero /wp-content/debug.log
define('WP_DEBUG_LOG', true);

// Desactiva mostrar los errores y avisos 
define('WP_DEBUG_DISPLAY', false);
@ini_set('display_errors',0);

// Usa versiones dev de ficheros centrales JS y CSS (solo necesario si estás modificando esos ficheros centrales)
define('SCRIPT_DEBUG', true);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
